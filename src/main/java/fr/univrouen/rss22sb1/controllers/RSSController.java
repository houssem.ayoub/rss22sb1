package fr.univrouen.rss22sb1.controllers;

import fr.univrouen.rss22sb1.model.Item;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class RSSController {
    @GetMapping("/resume")
    public String getListRSSinXML() {
        return "Envoi de la liste des flux RSS";
    }
    @GetMapping("/id")
    public String getRSSinXML(@RequestParam(value = "guid") String texte) {
            return ("Détail du flux RSS demandé : " + texte);
    }

    @GetMapping("/test")
    public String getRSSinTest(@RequestParam(value = "nb") String nb, @RequestParam(value = "search") String search) {
        return ("Test :\n guid ="+ nb+"\ntitre = "+search);
    }

    @RequestMapping(value = "/xml",produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public Item getXML() {
        return new Item("12345678","Test item","2022-05-01T11:22:33");
    }
}
